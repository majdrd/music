import { Action } from '@ngrx/store';
import { ISong } from './app.component';

export const ADD_SONG = 'ADD_SONG';
export const ADD_SONGS = 'ADD_SONGS';
export const UPDATE_SONG = 'UPDATE_SONG';
export const DELETE_SONG = 'DELETE_SONG';

export class AddSong implements Action {
    readonly type = ADD_SONG;
    // payload: ISong;
    constructor(public payload: ISong) {

    }
}
export class AddSongs implements Action {
    readonly type = ADD_SONGS;
    // payload: ISong;
    constructor(public payload: ISong[]) {

    }
}
export class UpdateSong implements Action {
    readonly type = UPDATE_SONG;
    // payload: ISong;
    constructor(public payload: {ID: string, song: ISong}) {

    }
}
export class DeleteSong implements Action {
    readonly type = DELETE_SONG;
    // payload: ISong;
    constructor(public payload: string) {

    }
}

export type SongListActions = AddSong | AddSongs | UpdateSong | DeleteSong;
