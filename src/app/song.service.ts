import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class SongsService {
    private songList: any;
    private JSON_URL = 'assets/songList.json';
    constructor(private httpModule: HttpClient) {
    }

    loadSongList(): Observable<any> {
        return this.loadJson(this.JSON_URL);
    }
    public loadJson(url: string): Observable<any> {
        return this.httpModule.get(url);
    }
}
