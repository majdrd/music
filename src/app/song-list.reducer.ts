import * as SongListActions from './songs-list.actions';

const initial = {
    songlist: [
        {
            ID: '_ujq7okwz3',
            Name: 'Say Something',
            Artist: 'Justin Timberlake',
            Genre: 'rock',
            CoverURL: 'https://i.ytimg.com/vi/ImFMl1K2A9A/maxresdefault.jpg',
            ReleaseDate: 'Jan 25, 2018'
        },
        {
            ID: '_ujq8okwz3',
            Name: 'Say Something',
            Artist: 'Justin Timberlake',
            Genre: 'rock',
            CoverURL: 'https://i.ytimg.com/vi/ImFMl1K2A9A/maxresdefault.jpg',
            ReleaseDate: new Date()
        }
    ]
};

export function songListReducer(state = initial, action: SongListActions.SongListActions) {
    switch (action.type) {
        case SongListActions.ADD_SONG:
            return {
                ...state,
                songlist: [...state.songlist, action.payload]
            };
        case SongListActions.ADD_SONGS:
            return {
                ...state,
                songlist: [...state.songlist, ...action.payload]
            };
        case SongListActions.UPDATE_SONG:
            const song = state.songlist.find(item => item.ID === action.payload.ID);
            const updatedSong = {
                ...song,
                ...action.payload.song
            };
            const updatedSongList = [...state.songlist];
            updatedSongList.forEach((item, index) => {
                if (item.ID === updatedSong.ID) {
                    updatedSongList[index] = updatedSong;
                    // item = updatedSong;
                }
            });
            return {
                ...state,
                songlist: updatedSongList
            };
        case SongListActions.DELETE_SONG:
            return {
                ...state,
                songlist: state.songlist.filter(item => item.ID !== action.payload)
            };
        default:
            return state;
    }

}
