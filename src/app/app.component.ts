import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { SongsService } from './song.service';
import { Store } from '@ngrx/store';
import * as SongListActions from './songs-list.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  tempList = [];
  musicList: Observable<any>;
  private musicListB: BehaviorSubject<any>;
  constructor(private songService: SongsService,
              private store: Store<{ songList: { songlist: [] } }>
  ) {

  }
  ngOnInit() {
    this.musicList = this.store.select('songList');
    // this.musicListB = new BehaviorSubject([]);
    // this.musicList = this.musicListB.asObservable();
    // this.songService.loadSongList().subscribe((list) => {
    //   console.log(list);

    //   this.tempList = list.songlist;
    //   this.musicListB.next(list.songlist);
    //   console.log(this.musicList);
    // });
  }
  private getUUID() {
    return '_' + Math.random().toString(36).substr(2, 9);
  }
  private addSong() {
    const id = this.getUUID();
    const song = {
      ID: id,
      Name: 'Say Something',
      Artist: 'Justin Timberlake',
      Genre: 'rock',
      CoverURL: 'https://i.ytimg.com/vi/ImFMl1K2A9A/maxresdefault.jpg',
      ReleaseDate: new Date()
    };
    this.store.dispatch(new SongListActions.AddSong(song));
    // this.addSong1(song);
  }
  private addSong1(song: ISong) {
    this.tempList.push(song);
    this.musicListB.next(this.tempList);
    console.log(this.tempList);
  }

  private updateSong() {
    const id = '_ujq7okwz3';
    const song1 = {
      ID: id,
      Name: 'Say Something3333',
      Artist: 'Justin Timberlake',
      Genre: 'rock',
      CoverURL: 'https://i.ytimg.com/vi/ImFMl1K2A9A/maxresdefault.jpg',
      ReleaseDate: new Date()
    };
    this.store.dispatch(new SongListActions.UpdateSong({
      ID: id,
      song: song1
    }));

  }
  private removeSong(songID: string) {
    console.log(songID);
    this.store.dispatch(new SongListActions.DeleteSong(songID));
    // this.tempList = this.tempList.filter(item => item.ID !== songID);
    // this.musicListB.next(this.tempList);

    // console.log(this.tempList);

  }
}




// ngOnInit() {
//   this.musicListB = new BehaviorSubject([]);
//   this.musicList = this.musicListB.asObservable();
//   this.songService.loadSongList().subscribe((list) => {
//     this.musicListB.next(list);
//     console.log(this.musicList);
//   });
// }

// private addSong(song: ISong) {
//   // this.musicList.push(song);
// }


export interface ISong {
  ID: string;
  Name: string;
  Artist: string;
  Genre: string;
  CoverURL: string;
  ReleaseDate: Date;

}
